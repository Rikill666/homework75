import {
    DECODED_CIPHER_ERROR,
    DECODED_CIPHER_REQUEST, DECODED_CIPHER_SUCCESS,
    ENCODED_CIPHER_ERROR, ENCODED_CIPHER_REQUEST,
    ENCODED_CIPHER_SUCCESS, ON_CHANGE_INFO,
} from "../actions/cipherActions";

const initialState = {
    encodedWord:"",
    decodedWord:"",
    error: null,
};

const cipherReducer = (state = initialState, action) => {
    switch (action.type) {
        case ENCODED_CIPHER_REQUEST:
            return {...state};
        case ENCODED_CIPHER_SUCCESS:
            return {...state, encodedWord: action.encodedWord, error:null};
        case ENCODED_CIPHER_ERROR:
            return {...state, error:action.error};
        case DECODED_CIPHER_REQUEST:
            return {...state};
        case DECODED_CIPHER_SUCCESS:
            return {...state, decodedWord: action.decodedWord, error:null};
        case DECODED_CIPHER_ERROR:
            return {...state,error:action.error};
        case ON_CHANGE_INFO:
            return {...state, [action.fieldName]: action.value};
        default:
            return state;
    }
};

export default cipherReducer;