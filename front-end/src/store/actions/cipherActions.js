import axiosApi from "../../axiosApi";

export const ENCODED_CIPHER_REQUEST = "ENCODED_CIPHER_REQUEST";
export const ENCODED_CIPHER_SUCCESS = "ENCODED_CIPHER_SUCCESS";
export const ENCODED_CIPHER_ERROR = "ENCODED_CIPHER_ERROR";
export const DECODED_CIPHER_REQUEST = "DECODED_CIPHER_REQUEST";
export const DECODED_CIPHER_SUCCESS = "DECODED_CIPHER_SUCCESS";
export const DECODED_CIPHER_ERROR = "DECODED_CIPHER_ERROR";

export const ON_CHANGE_INFO = "ON_CHANGE_INFO";

export function onChangeValue(value, name) {
    return {
        type: ON_CHANGE_INFO,
        fieldName: name,
        value: value
    };
}

export const encodedCipherRequest = () => {
    return {type: ENCODED_CIPHER_REQUEST};
};

export const encodedCipherSuccess = (encodedWord) => {
    return {type: ENCODED_CIPHER_SUCCESS, encodedWord};
};

export const encodedCipherError = (error) => {
    return {type: ENCODED_CIPHER_ERROR, error};
};

export const decodedCipherRequest = () => {
    return {type: DECODED_CIPHER_REQUEST};
};

export const decodedCipherSuccess = (decodedWord) => {
    return {type: DECODED_CIPHER_SUCCESS, decodedWord};
};

export const decodedCipherError = (error) => {
    return {type: DECODED_CIPHER_ERROR, error};
};

export const encodePostCipher = (cipher) => {
    return async dispatch => {
        try{
            dispatch(encodedCipherRequest());
            const response = await axiosApi.post('/cipher/encode', cipher);
            dispatch(encodedCipherSuccess(response.data.encoded));
        }
        catch (e) {
            dispatch(encodedCipherError(e));
        }
    };
};

export const decodePostCipher = (cipher) => {
    return async dispatch => {
        try{
            dispatch(decodedCipherRequest());
            const response = await axiosApi.post('/cipher/decode', cipher);
            dispatch(decodedCipherSuccess(response.data.decoded));
        }
        catch (e) {
            dispatch(decodedCipherError(e));
        }
    };
};