import React, {Component} from 'react';
import {connect} from "react-redux";
import {Col, Form, FormGroup, Input, Label, Row} from "reactstrap";
import {bindActionCreators} from "redux";
import {decodePostCipher, encodePostCipher} from "../../store/actions/cipherActions";
import * as cipherActions from "../../store/actions/cipherActions";
import './Cryptographer.css';
class Cryptographer extends Component {
    state = {
        password: "",
    };

    valueChanged = event => this.setState({[event.target.name]: event.target.value});

    encodingText = () => {
        if(this.state.password){
            const encodeCipher = {
                password:this.state.password,
                message:this.props.decodedWord
            };
            this.props.encodePostCipher(encodeCipher);
        }
    };

    decodingText = () => {
        if(this.state.password){
            const decodeCipher = {
                password:this.state.password,
                message:this.props.encodedWord
            };
            this.props.decodePostCipher(decodeCipher);
        }
    };

    render() {
        return (
            <Form style={{width: "50%", marginTop:"25px"}}>
                <FormGroup>
                    <Label for="decodedMessage">Decoded message</Label>
                    <Input
                        type="textarea"
                        name="decodedWord"
                        id="decodedMessage"
                        value={this.props.decodedWord}
                        onChange={e=>this.props.onChangeValue.onChangeValue(e.target.value, e.target.name)}
                    />
                </FormGroup>
                <Row>
                    <Col>
                        <FormGroup>
                            <Label for="examplePassword">Password</Label>
                            <Input
                                type="text"
                                name="password"
                                id="examplePassword"
                                value={this.state.password}
                                onChange={this.valueChanged}
                            />
                        </FormGroup>
                    </Col>
                    <Col>
                        <i onClick={this.encodingText} className="fas fa-arrow-down"/>
                        <i onClick={this.decodingText} className="fas fa-arrow-up"/>
                    </Col>
                </Row>
                <FormGroup>
                    <Label for="encodedMessage">Encoded message</Label>
                    <Input
                        type="textarea"
                        name="encodedWord"
                        id="encodedMessage"
                        value={this.props.encodedWord}
                        onChange={e=>this.props.onChangeValue.onChangeValue(e.target.value, e.target.name)}
                    />
                </FormGroup>
            </Form>
        );
    }
}

const mapStateToProps = state => {
    return {
        encodedWord:state.cipher.encodedWord,
        decodedWord:state.cipher.decodedWord,
        error: state.cipher.error
    };
};

const mapDispatchToProps = dispatch => {
    return {
        encodePostCipher: (cipher) => dispatch(encodePostCipher(cipher)),
        decodePostCipher: (cipher) => dispatch(decodePostCipher(cipher)),
        onChangeValue: bindActionCreators(cipherActions, dispatch)
    }
};
export default connect(mapStateToProps, mapDispatchToProps)(Cryptographer);