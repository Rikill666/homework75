import React from 'react';
import {Switch, Route} from "react-router-dom";
import {Container} from "reactstrap";
import Cryptographer from "./containers/Cryptographer/Cryptographer";

const App = () => {
  return (
      <Container style={{marginLeft:"25%"}}>
          <Switch>
            <Route path="/" exact component={Cryptographer}/>
            <Route render={() => <h1>Not Found</h1>}/>
          </Switch>
      </Container>
  );
};

export default App;
