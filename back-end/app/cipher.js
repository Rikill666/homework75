const express = require('express');
const Vigenere = require('caesar-salad').Vigenere;

const router = express.Router();


router.post('/encode', (req, res) => {
    const codedWord = Vigenere.Cipher(req.body.password).crypt(req.body.message);
    res.send({encoded: codedWord});
});

router.post('/decode', (req, res) => {
    const decodedWord = Vigenere.Decipher(req.body.password).crypt(req.body.message);
    res.send({decoded: decodedWord});
});

module.exports = router;