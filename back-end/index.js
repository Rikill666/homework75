const express = require('express');
const cors = require('cors');
const cipher = require('./app/cipher.js');

const app = express();
const port = 8000;

app.use(express.json());
app.use(cors());
app.use('/cipher', cipher);

app.listen(port, ()=>{
    console.log("Server started on " + port + " port");
});